<?php

namespace App\Policies;

use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class UserPolicy
{
    use HandlesAuthorization;

    public function before(?User $user)
    {
        if ($user && $user->isAdmin()) {
            return true;
        }
    }

    public function downloadAvatar(User $user, User $model)
    {
        return $user->id == $model->id;
    }

    public function viewAny(User $user)
    {
        return $user;
    }
}
