@extends('layouts.app')
@section('main')
    <form action="{{route('avatar.store',$user )}}" method="post" enctype="multipart/form-data">
        @csrf
        <div class="form-group row my-5">
            <input type="hidden" name="user" value="{{$user}}">
            <div class="col-md-6 offset-md-4">
                <div class="form-check">
                    <input class="form-check-input" type="file" name="avatar" }}>
                </div>
            </div>
        </div>
        <button type="submit">Download</button>
    </form>
@endsection
