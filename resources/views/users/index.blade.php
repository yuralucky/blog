@extends('layouts.app')
@section('main')
    <h4 class="mx-5">
        <a href="#">Users</a></h4>

    <table class="table table-dark">
        <thead>
        <tr>
            <th scope="col">Name</th>
            <th scope="col">Email</th>
            <th scope="col">Avatar</th>
        </tr>
        </thead>
        <tbody>
        @foreach($users as $user)
            @can('download-avatar',$user)
                <tr>
                    <th><a href="{{route('avatar.form',$user)}}">{{$user->name}}</a></th>
                    <td> {{$user->email}}</td>
                    <td><a src="#"><img src="{{asset('storage/app/avatars/'.$user->avatar)}}" alt="foto"></a></td>
                </tr>
            @endcan
        @endforeach
        </tbody>
    </table>


@endsection
