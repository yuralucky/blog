<?php

use App\Http\Controllers\ArticleController;
use App\Http\Controllers\auth\AuthController;
use App\Http\Controllers\UserController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Auth::routes(['verify' => true]);

Route::get('/', [ArticleController::class, 'main'])
    ->name('main')->middleware('guest');
Route::group(['middleware' => 'auth'], function () {
    Route::get('/articles', [ArticleController::class, 'index'])
        ->name('articles.index');
    Route::get('/articles/create', [ArticleController::class, 'create'])
        ->name('articles.create');
    Route::post('/articles', [ArticleController::class, 'store'])
        ->name('articles.store');
    Route::get('/articles/{article}/edit', [ArticleController::class, 'edit'])
        ->name('articles.edit');
    Route::post('/articles/{article}/edit', [ArticleController::class, 'update'])
        ->name('articles.update');
    Route::get('/articles/{article}/delete', [ArticleController::class, 'destroy'])
        ->name('articles.destroy');

    Route::get('/users/{user}/download', [UserController::class, 'createAvatar'])
        ->name('avatar.form');
    Route::get('/users/', [UserController::class, 'index'])
        ->name('users.index');
    Route::post('/users/{user}/download', [UserController::class, 'uploadAvatar'])
        ->name('avatar.store');
});


